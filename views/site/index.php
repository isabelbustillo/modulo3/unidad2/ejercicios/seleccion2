<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de Seleccion 1</h1>

        <p class="lead">Modulo 3 - Unidad 2</p>
    </div>

    <div class="body-content">
        <div class="row">
            <!-- 
            boton de consulta
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1</h3>
                        <p>Listar las edades de los ciclistas (sin repetidos)</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta1a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta1'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta
            -->
             <!--
            boton de consulta 2
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 2</h3>
                        <p>Listar las edades de los ciclistas de Artiach</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta2a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta2'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta 2
            -->
              <!--
            boton de consulta 3
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 3</h3>
                        <p>Listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta3a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta3'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta 3
            -->
              <!--
            boton de consulta 4
            -->
            </div>
            <div class="row">
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 4</h3>
                        <p>Listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta4a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta4'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta 4
            -->
              <!--
            boton de consulta 5
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 5</h3>
                        <p>Listar los dorsales de los ciclistas cuya edad este entre 28 y 32 y además que solo sean de Banesto</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta5a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta5'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta 5
            -->
               <!--
            boton de consulta 6
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 6</h3>
                        <p>Indicar el nombre de los ciclistas que el numero de caracteres del nombre sea mayor que 8</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta6a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta6'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta 6
            -->
            </div>
            <div class="row">
               <!--
            boton de consulta 7
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 7</h3>
                        <p>Listar el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas
                        que debe mostrar el nombre en mayúsculas</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta7a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta7'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta 7
            -->
             <!--
            boton de consulta 8
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 8</h3>
                        <p>Listar todos los ciclistas que han llevado el maillot MGE(amarillo) en alguna etapa</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta8a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta8'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta 8
            -->
            <!--
            boton de consulta 9
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 9</h3>
                        <p>Listar el nombre de los puertos cuya altura sea mayor que 1500</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta9a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta9'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta 9
            -->
            </div>
            <div class="row">
             <!--
            boton de consulta 10
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 10</h3>
                        <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura este entre 1800 y 3000</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta10a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta10'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta 10
            -->
             <!--
            boton de consulta 11
            -->
            <div class="col-sm-6 col-md-4">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 11</h3>
                        <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y 3000</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta11a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta11'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta 11
            -->
        </div>
    </div>
</div>
